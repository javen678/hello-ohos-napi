/*
* Copyright (c) 2021 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/**
 * The url module provides utilities for URL resolution and parsing.
 * @since 7
 * @devices phone, tablet
 * @import import helloModule from '@ohos.hellonapi';
 * @permission N/A
 */

declare namespace helloModule {
	function helloNapi(): string;
	function add(num1: number, num2: number): number;
	function addCallback(num1: number, num2: number, callback:(result: number) => void): void;
	function addPromise(num1: number, num2: number): Promise<number>;	
	function addAsync(num1: number, num2: number, callback:(result: number) => void): void;
	function addAsync(num1: number, num2: number): Promise<number>;
}

export default helloModule;