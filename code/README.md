# 源码说明

# mysubsys 目录

napi 扩展API代码，需分支到OpenHarmony系统源码根目录下，和系统源码一起构建。参考[深入浅出 OpenHarmony NAPI（1）——Hello NAPI](../doc/1.HelloNAPI.md)中的添加子系统组件章节操作。

# HelloNAPI 目录

标准系统应用源码，用于调用、测试扩展API。

# @ohos.hellonapi.d.ts 文件

接口定义.d.ts文件。